package buu.example.plusgameprojecttask4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import buu.example.plusgameprojecttask4.R
import buu.example.plusgameprojecttask4.databinding.FragmentDelBinding
import kotlinx.android.synthetic.main.fragment_del.*
import kotlin.random.Random
import kotlin.random.nextInt

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [delFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

private lateinit var binding: FragmentDelBinding

class delFragment : Fragment() {

    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentDelBinding>(inflater, R.layout.fragment_del, container, false)
        playGame()
        return binding.root

    }
    private fun playGame() {
        var number1 = Random.nextInt(1..10)
        var number2 = Random.nextInt(1..10)

        val sum = number1 - number2
        val sum2 = sum.toString()
        binding.apply {

            numberdelete1.text = number1.toString()
            numberdelete2.text = number2.toString()
            randomAns(sum)
            check(sum2)
        }
    }

    private fun randomAns(
        sum: Int
    ) {
        binding.apply {
            val ans = Random.nextInt(-1..1)
            if (ans == -1) {
                answerdeletebtn1.text = (sum + 0).toString()
                answerdeletebtn2.text = (sum + 1).toString()
                answerdeletebtn3.text = (sum + 2).toString()
            } else if (ans == 0) {
                answerdeletebtn1.text = (sum - 1).toString()
                answerdeletebtn2.text = (sum + 0).toString()
                answerdeletebtn3.text = (sum + 1).toString()
            } else if (ans == 1) {
                answerdeletebtn1.text = (sum - 2).toString()
                answerdeletebtn2.text = (sum - 1).toString()
                answerdeletebtn3.text = (sum + 0).toString()
            }
        }
    }

    private fun check(
        sum2: String
    ) {
        binding.apply {
            answerdeletebtn1.setOnClickListener {
                if (sum2 == answerdeletebtn1.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
            answerdeletebtn2.setOnClickListener {
                if (sum2 == answerdeletebtn2.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
            answerdeletebtn3.setOnClickListener {
                if (sum2 == answerdeletebtn3.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
        }
    }

    private fun sumF() {
        binding.apply {
            txt_del.text = "ไม่ถูกต้อง".toString()
            false_number.text = (false_number.text.toString().toInt() + 1).toString()
            plusIncorrect += 1
        }
    }
    private fun sumT() {
        binding.apply {
            txt_del.text = "ถูกต้อง".toString()
            true_number.text = (true_number.text.toString().toInt() + 1).toString()
            plusCorrect += 1
            playGame()
        }

    }
}