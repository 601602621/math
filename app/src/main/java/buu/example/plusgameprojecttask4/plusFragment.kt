package buu.example.plusgameprojecttask4

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.example.plusgameprojecttask4.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_plus.*
import kotlin.random.Random
import kotlin.random.nextInt

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class plusFragment : Fragment() {
    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private lateinit var binding: FragmentPlusBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentPlusBinding>(inflater, R.layout.fragment_plus, container, false)
        playGame()
        return binding.root

    }
    private fun playGame() {
        var number1 = Random.nextInt(1..10)
        var number2 = Random.nextInt(1..10)
        val sum = number1 + number2
        val sum2 = sum.toString()
        binding.apply {

            txtadd1.text = number1.toString()
            txtadd2.text = number2.toString()
            randomAns(sum)
            check(sum2)
//            intent.putExtra("true", plusCorrect)
//            intent.putExtra("false", plusIncorrect)
//            setResult(Activity.RESULT_OK,intent)
        }
    }

    private fun randomAns(
        sum: Int
    ) {
        binding.apply {
            val ans = Random.nextInt(-1..1)
            if (ans == -1) {
                btnadd1.text = (sum + 0).toString()
                btnadd2.text = (sum + 1).toString()
                btnadd3.text = (sum + 2).toString()
            } else if (ans == 0) {
                btnadd1.text = (sum - 1).toString()
                btnadd2.text = (sum + 0).toString()
                btnadd3.text = (sum + 1).toString()
            } else if (ans == 1) {
                btnadd1.text = (sum - 2).toString()
                btnadd2.text = (sum - 1).toString()
                btnadd3.text = (sum + 0).toString()
            }
        }
    }

    private fun check(
        sum2: String
    ) {
        binding.apply {
            btnadd1.setOnClickListener {
                if (sum2 == btnadd1.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
            btnadd2.setOnClickListener {
                if (sum2 == btnadd2.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
            btnadd3.setOnClickListener {
                if (sum2 == btnadd3.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
        }
    }

    private fun sumF() {
        binding.apply {
            resultsadd.text = "ไม่ถูกต้อง".toString()
            txtAnsaddF.text = (txtAnsaddF.text.toString().toInt() + 1).toString()
            plusIncorrect += 1
//            intent.putExtra("true", plusCorrect)
//            intent.putExtra("false", plusIncorrect)
//            setResult(Activity.RESULT_OK,intent)
        }
    }
    private fun sumT() {
        binding.apply {
            resultsadd.text = "ถูกต้อง".toString()
            txtAnsaddT.text = (txtAnsaddT.text.toString().toInt() + 1).toString()
            plusCorrect += 1
//            intent.putExtra("true", plusCorrect)
//            intent.putExtra("false", plusIncorrect)
//            setResult(Activity.RESULT_OK,intent)
            playGame()
        }

    }
}