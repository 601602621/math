package buu.example.plusgameprojecttask4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import buu.example.plusgameprojecttask4.R
import buu.example.plusgameprojecttask4.databinding.FragmentMulBinding
import kotlin.random.Random
import kotlin.random.nextInt

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [mulFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class mulFragment : Fragment() {
    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private lateinit var binding: FragmentMulBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMulBinding>(inflater, R.layout.fragment_mul, container, false)
        playGame()
        return binding.root

    }
    private fun playGame() {
        var number1 = Random.nextInt(1..10)
        var number2 = Random.nextInt(1..10)
        val sum = number1 * number2
        val sum2 = sum.toString()
        binding.apply {

            txtmul1.text = number1.toString()
            txtmul2.text = number2.toString()
            randomAns(sum)
            check(sum2)
        }
    }

    private fun randomAns(
        sum: Int
    ) {
        binding.apply {
            val ans = Random.nextInt(-1..1)
            if (ans == -1) {
                btnmul1.text = (sum + 0).toString()
                btnmul2.text = (sum + 1).toString()
                btnmul3.text = (sum + 2).toString()
            } else if (ans == 0) {
                btnmul1.text = (sum - 1).toString()
                btnmul2.text = (sum + 0).toString()
                btnmul3.text = (sum + 1).toString()
            } else if (ans == 1) {
                btnmul1.text = (sum - 2).toString()
                btnmul2.text = (sum - 1).toString()
                btnmul3.text = (sum + 0).toString()
            }
        }
    }

    private fun check(
        sum2: String
    ) {
        binding.apply {
            btnmul1.setOnClickListener {
                if (sum2 == btnmul1.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
            btnmul2.setOnClickListener {
                if (sum2 == btnmul2.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
            btnmul3.setOnClickListener {
                if (sum2 == btnmul3.text) {
                    sumT()
                } else {
                    sumF()
                }
            }
        }
    }

    private fun sumF() {
        binding.apply {
            resultsmul.text = "ไม่ถูกต้อง".toString()
            txtAnsmulF.text = (txtAnsmulF.text.toString().toInt() + 1).toString()
            plusIncorrect += 1
        }
    }
    private fun sumT() {
        binding.apply {
            resultsmul.text = "ถูกต้อง".toString()
            txtAnsmulT.text = (txtAnsmulT.text.toString().toInt() + 1).toString()
            plusCorrect += 1
            playGame()
        }

    }
}